#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <stack>
#include <cmath>

#define TYPE int
#define IT_TYPE long

using namespace std;

class Call {
public:
    IT_TYPE first; // this first and second naming comes from using a pair with only this two before
    IT_TYPE second;
    long level;
    Call(IT_TYPE first_, IT_TYPE second_, long level_):
        first(first_),
        second(second_),
        level(level_) {}
    Call() {}
};

void debug_print(vector<TYPE> &vec) {
    for (TYPE t : vec) {
        cout << t << "  ";
    }
    cout << endl;
}

void selection_sort(vector<TYPE> &tab, IT_TYPE begit, IT_TYPE endit) {
    IT_TYPE minit;
    IT_TYPE sortit;
    IT_TYPE curit;
    for (sortit = begit; sortit < endit; ++sortit) {
        minit = sortit;
        for (curit = sortit + 1; curit < endit + 1; ++curit) {
            if (tab[curit] < tab[minit]) minit = curit;
        }
        swap(tab[minit], tab[sortit]);
    }
}

IT_TYPE med3(vector<TYPE> vec, IT_TYPE a, IT_TYPE b, IT_TYPE c) {
    if (vec[b] < vec[a]) {
        if (vec[c] < vec[b]) return b;
        else {
            if (vec[c] < vec[a]) return c;
            else return a;
        }
    }
    else {
        if (vec[c] < vec[a]) return a;
        else {
            if (vec[c] < vec[b]) return c;
            else return b;
        }
    }
}

// integer overflow safe average
// a must be smaller than b
// for efficiency it's not checked here
inline IT_TYPE avg(IT_TYPE a, IT_TYPE b) {
    return a + (b-a)/2;
}

void quicksort_atom(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend, long level, stack<Call> &callstack) {
    IT_TYPE itpivot;
    IT_TYPE itwall;
    IT_TYPE it;
    if (itbegin < itend) {
        itpivot = med3(vec, itbegin, itend, avg(itbegin, itend));
        swap(vec[itbegin], vec[itend]);
        itpivot = itend;
        itwall = itbegin - 1; // ??? wall?
        for (it = itbegin; it < itend; ++it) {
            if (vec[it] < vec[itpivot]) {
                ++itwall;
                swap(vec[itwall], vec[it]);
            }
        }
        if (vec[itend] < vec[itwall + 1]) {
            swap(vec[itwall + 1], vec[itend]);
        }
        // p = it+1
        callstack.push(Call(itbegin, itwall, level + 1));
        callstack.push(Call(itwall + 2, itend, level + 1));
    }
}

void quicksort(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend) {
    Call call;
    stack<Call> callstack;
    callstack.push(Call(itbegin, itend, 0));
    while (!callstack.empty()) {
        call = callstack.top();
        callstack.pop();
        quicksort_atom(vec, call.first, call.second, call.level, callstack);
    }
}

// returns heap parent index of the node
inline IT_TYPE heap_parent(IT_TYPE node) {
    return (node == 0) ? 0 : (node-1)/2;
}

inline IT_TYPE heap_kid(IT_TYPE node, int which) {
    return 2*node + which;
}

void heapify(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend) {
    IT_TYPE it;
    IT_TYPE itup, itdown;
    for (it = itbegin + 1; it <= itend; ++it) {
        itdown = it;
        itup = heap_parent(it);
        while (vec[itup] < vec[itdown]) {
            swap(vec[itup], vec[itdown]);
            itdown = itup;
            itup = heap_parent(itup);
        }
    }
}

IT_TYPE max3(vector<TYPE> vec, IT_TYPE a, IT_TYPE b, IT_TYPE c) {
    if (vec[b] < vec[a]) {
        if (vec[c] < vec[b]) return a;
        else {
            if (vec[c] < vec[a]) return a;
            else return c;
        }
    }
    else {
        if (vec[c] < vec[a]) return b;
        else {
            if (vec[c] < vec[b]) return b;
            else return c;
        }
    }
}

IT_TYPE largest_in_family(vector<TYPE> &vec, IT_TYPE itparent, IT_TYPE itend) {
    if (heap_kid(itparent, 2) <= itend) {
        return max3(vec, itparent, heap_kid(itparent, 1), heap_kid(itparent, 2));
    }
    else if (heap_kid(itparent, 1) <= itend) {
        return max3(vec, itparent, itparent, heap_kid(itparent, 1));
    }
    return itparent;
}

// repairs the stack after first <-> last swap
void heap_repair(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend) {
    IT_TYPE itlargest;
    IT_TYPE itcurrent;

    itcurrent = itbegin;
    itlargest = largest_in_family(vec, itcurrent, itend);
    while (itlargest != itcurrent) {
        swap(vec[itcurrent], vec[itlargest]);
        itcurrent = itlargest;
        itlargest = largest_in_family(vec, itcurrent, itend);
    }
}

void heapsort(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend) {
    IT_TYPE itlast;

    heapify(vec, itbegin, itend);
    for (itlast = itend; itlast != itbegin;) {
        swap(vec[itbegin], vec[itlast]);
        --itlast;
        heap_repair(vec, itbegin, itlast);
    }
}

long int_log_2(long x) {
    long ret = 0;
    while (x >>= 1) ++ret;
    return ret;
}

void introsort(vector<TYPE> &vec, IT_TYPE itbegin, IT_TYPE itend) {
    Call call;
    stack<Call> callstack;
    long heap_level = floor(2.0*log(itend - itbegin)/log(2));
    callstack.push(Call(itbegin, itend, 0));
    while (!callstack.empty()) {
        call = callstack.top();
        callstack.pop();
        if (call.second <= call.first);
        else if (call.second - call.first <= 3) {
            selection_sort(vec, call.first, call.second);
        }
        else if (call.level > heap_level) {
            heapsort(vec, call.first, call.second);
        }
        else quicksort_atom(vec, call.first, call.second, call.level, callstack);
    }
}

int main(int argc, char** argv)
{
    ifstream input;
    ofstream output;
    TYPE tmp;
    vector<TYPE> vec;

    // checking if the invocation is correct
    if (argc != 2) {
        cout << "There should be exactly one argument" << endl;
        return -1;
    }

    // opening input file
    input.open(argv[1], ios::in);
    if (!input.good()) {
        cout << "Failed to open " << argv[1] << endl;
        return -1;
    }

    // opening output file
    output.open(strcat(argv[1], ".out"), ios::out);
    if (!output.good()) {
        cout << "Failed to open output file" << endl;
        return -1;
    }

    input >> tmp;
    while (!input.eof()) {
        vec.push_back(tmp);
        input >> tmp;
    }

    introsort(vec, 0, vec.size() - 1);

    for (TYPE tmp : vec) {
        output << tmp << endl;
    }

    return 0;
}
